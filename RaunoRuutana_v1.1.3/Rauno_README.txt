Peli: Rauno Ruutana

Tekij�t: Jonne Niskanen, Markus Tuominen & Roosa Penttinen

Peliss� ohjataan Rauno Ruutanaa nuolin�pp�imill�. V�lily�nnist� saa k�yt��n erikoiskyvyn jolla p��see verkkojen l�pi. Kyky latautuu hitaasti itsest��n, mutta sit� saa lis�� juomalla simaa. Munkeista ja simasta saa pisteit�. Lis�ohjeita l�ytyy itse pelist� "Ohjeet"-kohdassa. 

Sovellus k�ynnistyy omasta exe-tiedostosta. Sovellus toimii vain 16:9 -resoluutioilla. 