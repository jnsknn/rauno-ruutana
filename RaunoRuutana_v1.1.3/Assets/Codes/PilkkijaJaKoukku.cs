﻿using UnityEngine;
using System.Collections;

public class PilkkijaJaKoukku : MonoBehaviour{

	public float maxScale = 0.7f;
	public float minScale = 0.5f;
	public float maxPositionY = 2.51f;
	public float minPositionY = 2.5f;
	public float maxHookDistance = 6f;
	public float minHookDistance = 2f;

	private GameObject kamera = null;
	private GameObject koukku = null;

	// Use this for initialization
	void Start()
	{

		this.kamera = GameObject.Find("kamera");
		this.koukku = GameObject.Find ("koukku");

		setKinematic (true);

		spawnPilkkijaJaKoukku ();

	}

	// Update is called once per frame
	void Update()
	{

		this.GetComponent<Transform> ().position += Vector3.left * (kamera.GetComponent<Kamera> ().backgroundSpeed * kamera.GetComponent<Kamera> ().backgroundSpeedFix) * Time.deltaTime;
		if (this.GetComponent<Transform> ().position.x < 9.7f && this.GetComponent<Transform> ().position.x > -9.7f) {
			this.setKinematic (false);
		} else {
			this.setKinematic (true);
		}

		if(this.GetComponent<Transform>().position.x < kamera.GetComponent<Kamera>().objDestroyLimit){
			spawnPilkkijaJaKoukku ();
		}

	}

	private void setKinematic(bool isKinematic){
		this.koukku.GetComponent<Rigidbody2D> ().isKinematic = isKinematic;
	}

	private bool getKinematic(){
		return this.koukku.GetComponent<Rigidbody2D> ().isKinematic;
	}

	public void spawnPilkkijaJaKoukku(){
		
		float scale = Random.Range(this.minScale, this.maxScale);
		this.GetComponent<Transform>().localScale = new Vector2(scale, scale);

		this.GetComponent<Transform>().position = new Vector2(Random.Range(this.kamera.GetComponent<Kamera>().objMaxSpawn, this.kamera.GetComponent<Kamera>().objSpawnLimit),
			Random.Range(this.minPositionY, this.maxPositionY));

		this.koukku.GetComponent<DistanceJoint2D> ().distance = Random.Range (this.minHookDistance, this.maxHookDistance);

		float randomFlip = Random.Range(0f, 1.1f);

		if(randomFlip > 0.5f){
			this.GetComponent<Transform> ().Rotate(0f, 180f, 0f);
		}

	}

	public void setMaxScale(float maxScale)
	{
		this.maxScale = maxScale;
	}

	public float getMaxScale()
	{
		return this.maxScale;
	}

	public void setMinScale(float minScale)
	{
		this.minScale = minScale;
	}

	public float getMinScale()
	{
		return this.minScale;
	}

	public void setMaxPositionY(float maxPositionY)
	{
		this.maxPositionY = maxPositionY;
	}

	public float getMaxPositionY()
	{
		return this.maxPositionY;
	}

	public void setMinPositionY(float minPositionY)
	{
		this.minPositionY = minPositionY;
	}

	public float getMinPositionY()
	{
		return this.minPositionY;
	}
}
