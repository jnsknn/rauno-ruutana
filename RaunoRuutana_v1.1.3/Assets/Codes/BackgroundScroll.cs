﻿using UnityEngine;
using System.Collections;

public class BackgroundScroll : MonoBehaviour {
	
	private GameObject kamera = null;

    // Use this for initialization
    void Start(){
		this.kamera = GameObject.Find ("kamera");
    }

    // Update is called once per frame
    void Update(){
		GetComponent<Renderer>().material.mainTextureOffset += Vector2.right * kamera.GetComponent<Kamera>().backgroundSpeed * Time.deltaTime;
    }
}

