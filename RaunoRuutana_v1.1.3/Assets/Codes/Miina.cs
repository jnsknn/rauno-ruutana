﻿using UnityEngine;
using System.Collections;

public class Miina : MonoBehaviour{

	public float maxScale = 0.7f;
	public float minScale = 0.4f;
	public float maxPositionY = 0.91f;
	public float minPositionY = -2.92f;

	private GameObject kamera = null;
	public GameObject ketju;
	public GameObject ketju1;
	public GameObject ketju2;

	// Use this for initialization
	void Start()
	{

		this.kamera = GameObject.Find("kamera");

		setKinematic (true);

		spawnMiina ();

	}

	// Update is called once per frame
	void Update()
	{

		this.GetComponent<Transform> ().position += Vector3.left * (kamera.GetComponent<Kamera> ().backgroundSpeed * kamera.GetComponent<Kamera> ().backgroundSpeedFix) * Time.deltaTime;

		if (this.GetComponent<Transform> ().position.x < 9.7f && this.GetComponent<Transform> ().position.x > -7.8f) {
			this.setKinematic (false);
		} else {
			this.setKinematic (true);
		}

		if(this.GetComponent<Transform>().position.x < kamera.GetComponent<Kamera>().objDestroyLimit){
			spawnMiina ();
		}

	}

	private void setKinematic(bool isKinematic){
		this.ketju.GetComponent<Rigidbody2D> ().isKinematic = isKinematic;
		this.ketju1.GetComponent<Rigidbody2D> ().isKinematic = isKinematic;
		this.ketju2.GetComponent<Rigidbody2D> ().isKinematic = isKinematic;
		this.GetComponent<Rigidbody2D> ().isKinematic = isKinematic;
	}

	private bool getKinematic(){
		return this.GetComponent<Rigidbody2D> ().isKinematic;
	}

	public void spawnMiina(){

		float scale = Random.Range(this.minScale, this.maxScale);
		this.GetComponent<Transform>().localScale = new Vector2(scale, scale);

		this.GetComponent<Transform>().position = new Vector2(Random.Range(this.kamera.GetComponent<Kamera>().objMaxSpawn, this.kamera.GetComponent<Kamera>().objSpawnLimit),
			Random.Range(this.minPositionY, this.maxPositionY));

		float randomFlip = Random.Range(0f, 1.1f);

		if(randomFlip > 0.5f){
			this.GetComponent<Transform> ().Rotate(0f, 180f, 0f);
		}

	}

	public void setMaxScale(float maxScale)
	{
		this.maxScale = maxScale;
	}

	public float getMaxScale()
	{
		return this.maxScale;
	}

	public void setMinScale(float minScale)
	{
		this.minScale = minScale;
	}

	public float getMinScale()
	{
		return this.minScale;
	}

	public void setMaxPositionY(float maxPositionY)
	{
		this.maxPositionY = maxPositionY;
	}

	public float getMaxPositionY()
	{
		return this.maxPositionY;
	}

	public void setMinPositionY(float minPositionY)
	{
		this.minPositionY = minPositionY;
	}

	public float getMinPositionY()
	{
		return this.minPositionY;
	}
}
