﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	static GameManager Instance;

	private GameObject ruutana = null;

	private bool initAdd = false;

	private List<int> highScores = new List<int>();

	private int score = 0;

	private bool initScores = false;

	// Use this for initialization
	void Start () {

		if (Instance != null) {
			Destroy (this.gameObject);
		} else {
			DontDestroyOnLoad (gameObject);
			Instance = this;
		}
	}
	
	// Update is called once per frame
	void Update () {

		if(SceneManager.GetActiveScene().name.Equals("peliskene")){

			if (this.ruutana == null) {
				this.ruutana = findRuutana ();
			} else {
				this.score = this.ruutana.GetComponent<Ruutana> ().score;
				this.initAdd = true;
			}
		}

        if (SceneManager.GetActiveScene().name.Equals("menuskene") && this.initAdd) {
            addToHighScores(this.score);
            this.initScores = true;
        }
        else if (SceneManager.GetActiveScene().name.Equals("menuskene")) {
            this.initScores = true;
        }

		if(SceneManager.GetActiveScene().name.Equals("pisteskene") && this.initScores){
			displayScores ();
		}
	
	}

	private GameObject findRuutana(){
		return GameObject.Find ("ruutana");
	}

	private void addToHighScores(int score){
		this.highScores.Add (score);
		this.initAdd = false;
	}

	private void displayScores(){

		try{

			this.highScores.Sort ((a, b) => -1*a.CompareTo(b));

			for(int i = 0;i<this.highScores.Count;i++){

				if(i == 0){
					GameObject.Find ("first").GetComponent<TextMesh>().text = "1. " + this.highScores[0].ToString() + " pistettä";
				}

				if(i == 1){
					GameObject.Find ("second").GetComponent<TextMesh>().text = "2. " + this.highScores[1].ToString() + " pistettä";
				}

				if(i == 2){
					GameObject.Find ("third").GetComponent<TextMesh>().text = "3. " + this.highScores[2].ToString() + " pistettä";
				}	
			}

			this.initScores = false;
			
		}catch{}
	}
}
