﻿using UnityEngine;
using System.Collections;

public class Hauki : MonoBehaviour {

	public float maxScale = 0.5f;
	public float minScale = 0.4f;
	public float maxPositionY = 1.08f;
	public float minPositionY = -3.34f;

	public int spawnFrames = 4000;

	private GameObject kamera = null;
    public GameObject defaultGoalObj;
    private Transform defaultGoal = null;
	private GameObject goalObj = null;
	private Transform goal = null;

	private float speed = 3f;

	// Use this for initialization
	void Start () {

		kamera = GameObject.Find ("kamera");

		float scale = Random.Range (this.minScale, this.maxScale);
		this.GetComponent<Transform> ().localScale = new Vector2 (scale, scale);

		this.GetComponent<Transform> ().position = new Vector2 (Random.Range (this.kamera.GetComponent<Kamera>().objDestroyLimit, this.kamera.GetComponent<Kamera> ().objMinSpawn),
			Random.Range (this.minPositionY, this.maxPositionY));

        this.defaultGoal = defaultGoalObj.GetComponent<Transform>();

		this.goalObj = GameObject.Find ("ruutana");
		this.goal = goalObj.GetComponent<Transform> ();

	}

	// Update is called once per frame
	void Update () {

        if (this.goalObj.GetComponent<Ruutana>().isHiding){

            this.GetComponent<Transform>().position = Vector3.MoveTowards(this.GetComponent<Transform>().position, defaultGoal.position, this.speed * Time.deltaTime);

            this.GetComponent<SpriteRenderer>().flipX = false;

        }else {

            this.GetComponent<Transform>().position = Vector3.MoveTowards(this.GetComponent<Transform>().position, goal.position, this.speed * Time.deltaTime);

            if (this.goal.position.x < this.GetComponent<Transform>().position.x)
            {
                this.GetComponent<SpriteRenderer>().flipX = true;
            }
            else {
                this.GetComponent<SpriteRenderer>().flipX = false;
            }

        }

		if(this.GetComponent<Transform>().position.x > kamera.GetComponent<Kamera> ().objMaxSpawn){
			Destroy (this.gameObject);
		}

	}

	public void setMaxScale(float maxScale){
		this.maxScale = maxScale;
	}

	public float getMaxScale(){
		return this.maxScale;
	}

	public void setMinScale(float minScale){
		this.minScale = minScale;
	}

	public float getMinScale(){
		return this.minScale;
	}

	public void setMaxPositionY(float maxPositionY){
		this.maxPositionY = maxPositionY;
	}

	public float getMaxPositionY(){
		return this.maxPositionY;
	}

	public void setMinPositionY(float minPositionY){
		this.minPositionY = minPositionY;
	}

	public float getMinPositionY(){
		return this.minPositionY;
	}

	public void setSpawnFrames(int spawnFrames){
		this.spawnFrames = spawnFrames;
	}

	public int getSpawnFrames(){
		return this.spawnFrames;
	}

	public void setSpeed(float speed){
		this.speed = speed;
	}

	public float getSpeed(){
		return this.speed;
	}

	public void setGoalObj(GameObject goalObj){
		this.goalObj = goalObj;
	}

	public GameObject getGoalObj(){
		return this.goalObj;
	}
}
