﻿using UnityEngine;
using System.Collections;

public class MenuSounds : MonoBehaviour {

    public AudioSource[] sounds = null;
    public AudioSource sound1 = null;
    public AudioSource sound2 = null;
    public AudioSource sound3 = null;

    // Use this for initialization
    void Start () {

        this.sounds = this.GetComponents<AudioSource>();
        this.sound1 = this.sounds[0];
        this.sound2 = this.sounds[1];
        this.sound3 = this.sounds[2];

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseDown(){
        float roll = Random.Range(0f, 1.1f);

        if (roll > 0.5f){
            this.sound1.Play();
        }
        else
        {
            this.sound2.Play();
        }
    }

    void OnMouseEnter()
    {
        this.sound3.Play();
    }

}
