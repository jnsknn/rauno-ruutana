﻿using UnityEngine;
using System.Collections;

public class SpawnControl : MonoBehaviour {

	public GameObject arkku;
	private int arkkuCounter;

	public GameObject isoKivi;
	private int isoKiviCounter;

	public GameObject vesiheina;
	private int vesiheinaCounter;

    public GameObject hauki;
    private int haukiCounter;

    public GameObject hukkunut1;
    private int hukkunut1Counter;

	public GameObject muovipussi;
	private int muovipussiCounter;

	public GameObject verkko;
	private int verkkoCounter;

	public GameObject munkki;
	private int munkkiCounter;

	public GameObject sima;
	private int simaCounter;

    // Use this for initialization
    void Start () {
		
		this.arkkuCounter = 0;
		this.isoKiviCounter = 0;
		this.vesiheinaCounter = 0;
        this.haukiCounter = 0;
        this.hukkunut1Counter = 0;
		this.muovipussiCounter = 0;
		this.verkkoCounter = 0;
		this.munkkiCounter = 0;
		this.simaCounter = 0;

		arkku.GetComponent<Arkku> ().setSpawnFrames (2000);
		isoKivi.GetComponent<Isokivi> ().setSpawnFrames (400);
		vesiheina.GetComponent<Vesiheina> ().setSpawnFrames (100);
        hauki.GetComponent<Hauki>().setSpawnFrames(3000);
		hukkunut1.GetComponent<Hukkunut1>().setSpawnFrames(2320);
		muovipussi.GetComponent<Muovipussi>().setSpawnFrames(800);
		verkko.GetComponent<Verkko> ().setSpawnFrames (700);
		munkki.GetComponent<Munkki> ().setSpawnFrames (100);
		sima.GetComponent<Sima> ().setSpawnFrames (500);

        spawnArkku ();
		spawnIsoKivi ();
		spawnVesiheina ();
        //spawnHauki();
        spawnHukkunut1();
		spawnMuovipussi ();
		spawnVerkko ();
		spawnMunkki ();
		spawnSima ();

	}
	
	// Update is called once per frame
	void Update () {

		this.arkkuCounter++;
		this.isoKiviCounter++;
		this.vesiheinaCounter++;
        this.haukiCounter++;
        this.hukkunut1Counter++;
		this.muovipussiCounter++;
		this.verkkoCounter++;
		this.munkkiCounter++;
		this.simaCounter++;

		if(this.arkkuCounter > arkku.GetComponent<Arkku>().getSpawnFrames()){
			spawnArkku ();
			this.arkkuCounter = 0;
		}

		if(this.isoKiviCounter > isoKivi.GetComponent<Isokivi>().getSpawnFrames()){
			spawnIsoKivi ();
			this.isoKiviCounter = 0;
		}

		if(this.vesiheinaCounter > vesiheina.GetComponent<Vesiheina>().getSpawnFrames()){
			spawnVesiheina ();
			this.vesiheinaCounter = 0;
		}

        if (this.haukiCounter > hauki.GetComponent<Hauki>().getSpawnFrames()) {
            int randomFrames = (int)Random.Range(3000, 5000);
            hauki.GetComponent<Hauki>().setSpawnFrames(randomFrames);
            spawnHauki();
            this.haukiCounter = 0;
        }

        if (this.hukkunut1Counter > hukkunut1.GetComponent<Hukkunut1>().getSpawnFrames()) {
            float randomRotateSpeed = Random.Range(0f, 1f);
            hukkunut1.GetComponent<Hukkunut1>().setRotateSpeed(randomRotateSpeed);
            spawnHukkunut1();
            this.hukkunut1Counter = 0;
        }

		if (this.muovipussiCounter > muovipussi.GetComponent<Muovipussi>().getSpawnFrames()) {
			float randomSpeed = Random.Range(2f, 4f);
			muovipussi.GetComponent<Muovipussi>().setSpeed(randomSpeed);
			spawnMuovipussi();
			this.muovipussiCounter = 0;
		}

		if (this.verkkoCounter > verkko.GetComponent<Verkko>().getSpawnFrames()) {
            int randomFrames = (int)Random.Range(300, 1700);
            verkko.GetComponent<Verkko>().setSpawnFrames(randomFrames);
            spawnVerkko();
			this.verkkoCounter = 0;
		}

		if (this.munkkiCounter > munkki.GetComponent<Munkki>().getSpawnFrames()) {
			float randomRotateSpeed = Random.Range(0f, 1f);
			float randomSpeed = Random.Range(2f, 4f);
			munkki.GetComponent<Munkki>().setSpeed(randomSpeed);
			munkki.GetComponent<Munkki>().setRotateSpeed(randomRotateSpeed);
			spawnMunkki();
			this.munkkiCounter = 0;
		}

		if (this.simaCounter > sima.GetComponent<Sima>().getSpawnFrames()) {
			float randomRotateSpeed = Random.Range(0f, 1f);
			float randomSpeed = Random.Range(1f, 2f);
			sima.GetComponent<Sima>().setSpeed(randomSpeed);
			sima.GetComponent<Sima>().setRotateSpeed(randomRotateSpeed);
			spawnSima();
			this.simaCounter = 0;
		}
	
	}

	public void spawnArkku(){
		Instantiate (arkku, new Vector2(0f, -100f), Quaternion.identity);
	}

	public void spawnIsoKivi(){
		Instantiate (isoKivi, new Vector2(0f, -100f), Quaternion.identity);
	}

	public void spawnVesiheina(){
		Instantiate (vesiheina, new Vector2(0f, -100f), Quaternion.identity);
	}

    public void spawnHauki() {
		Instantiate(hauki, new Vector2(0f, -100f), Quaternion.identity);
    }

    public void spawnHukkunut1(){
		Instantiate(hukkunut1, new Vector2(0f, -100f), Quaternion.identity);
    }

	public void spawnMuovipussi(){
		Instantiate(muovipussi, new Vector2(0f, -100f), Quaternion.identity);
	}

	public void spawnVerkko(){
		Instantiate(verkko, new Vector2(0f, -100f), Quaternion.identity);
	}

	public void spawnMunkki(){
		Instantiate(munkki, new Vector2(0f, -100f), Quaternion.identity);
	}

	public void spawnSima(){
		Instantiate(sima, new Vector2(0f, -100f), Quaternion.identity);
	}
}
