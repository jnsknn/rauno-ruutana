﻿using UnityEngine;
using System.Collections;

public class Ruutana : MonoBehaviour {

	private float speed = 3f;
	private float boostLeft = 200f;
	private float boostMultiplier = 2f;
	private float boostLossMultiplier = 1f;
	private float boostRegain = 0.05f;
	private float boostRegainOnDrink = 15f;

	public int score = 0;

	private bool isBoosting = false;

    public bool isHiding = false;

    public bool isGameOver = false;

	private AudioSource[] sounds = null;
	private AudioSource blobSound = null;
	private AudioSource burbSound1 = null;
	private AudioSource burbSound2 = null;
	private AudioSource fartSound1 = null;
	private AudioSource fartSound2 = null;
	private AudioSource eatingSound = null;

	private GameObject lblScore = null;
	private GameObject lblAbility = null;

    // Use this for initialization
    void Start () {
		this.sounds = this.GetComponents<AudioSource> ();
		this.blobSound = this.sounds[0];
		this.burbSound1 = this.sounds[1];
		this.burbSound2 = this.sounds[2];
		this.fartSound1 = this.sounds[3];
		this.fartSound2 = this.sounds[4];
		this.eatingSound = this.sounds[5];

		this.GetComponentInChildren<ParticleSystem> ().enableEmission = false;

		this.lblScore = GameObject.Find ("lblScore");
		this.lblAbility = GameObject.Find ("lblAbility");
	}
	
	// Update is called once per frame
	void Update () {

        if (!this.isGameOver)
		{
			if(this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("ruutana_suuauki")){
				this.GetComponent<Animator> ().SetBool ("isEating", false);
			}
			
			if(this.boostLeft > 190f)
				this.lblAbility.GetComponent<TextMesh> ().text = "||||||||||||||||||||";
			else if(this.boostLeft <= 190f && this.boostLeft > 180f)
				this.lblAbility.GetComponent<TextMesh> ().text = "|||||||||||||||||||";
			else if(this.boostLeft <= 180f && this.boostLeft > 170f)
				this.lblAbility.GetComponent<TextMesh> ().text = "||||||||||||||||||";
			else if(this.boostLeft <= 170f && this.boostLeft > 160f)
				this.lblAbility.GetComponent<TextMesh> ().text = "|||||||||||||||||";
			else if(this.boostLeft <= 160f && this.boostLeft > 150f)
				this.lblAbility.GetComponent<TextMesh> ().text = "||||||||||||||||";
			else if(this.boostLeft <= 150f && this.boostLeft > 140f)
				this.lblAbility.GetComponent<TextMesh> ().text = "||||||||||||||";
			else if(this.boostLeft <= 140f && this.boostLeft > 130f)
				this.lblAbility.GetComponent<TextMesh> ().text = "|||||||||||||";
			else if(this.boostLeft <= 130f && this.boostLeft > 120f)
				this.lblAbility.GetComponent<TextMesh> ().text = "||||||||||||";
			else if(this.boostLeft <= 120f && this.boostLeft > 110f)
				this.lblAbility.GetComponent<TextMesh> ().text = "|||||||||||";
			else if(this.boostLeft <= 110f && this.boostLeft > 100f)
				this.lblAbility.GetComponent<TextMesh> ().text = "||||||||||";
			else if(this.boostLeft <= 100f && this.boostLeft > 90f)
				this.lblAbility.GetComponent<TextMesh> ().text = "|||||||||";
			else if(this.boostLeft <= 90f && this.boostLeft > 80f)
				this.lblAbility.GetComponent<TextMesh> ().text = "||||||||";
			else if(this.boostLeft <= 80f && this.boostLeft > 70f)
				this.lblAbility.GetComponent<TextMesh> ().text = "|||||||";
			else if(this.boostLeft <= 70f && this.boostLeft > 60f)
				this.lblAbility.GetComponent<TextMesh> ().text = "||||||";
			else if(this.boostLeft <= 60f && this.boostLeft > 50f)
				this.lblAbility.GetComponent<TextMesh> ().text = "|||||";
			else if(this.boostLeft <= 50f && this.boostLeft > 40f)
				this.lblAbility.GetComponent<TextMesh> ().text = "||||";
			else if(this.boostLeft <= 40f && this.boostLeft > 30f)
				this.lblAbility.GetComponent<TextMesh> ().text = "|||";
			else if(this.boostLeft <= 30f && this.boostLeft > 20f)
				this.lblAbility.GetComponent<TextMesh> ().text = "||";
			else if(this.boostLeft <= 20f && this.boostLeft > 10f)
				this.lblAbility.GetComponent<TextMesh> ().text = "|";
			else
				this.lblAbility.GetComponent<TextMesh> ().text = "";

			if(this.boostLeft < 200f && !this.isBoosting){
				this.boostLeft += this.boostRegain;
			}

			if (this.isBoosting) {
				this.boostLeft -= this.boostLossMultiplier;
				this.GetComponent<Animator> ().speed = 2f;
				this.GetComponentInChildren<ParticleSystem> ().enableEmission = true;
			} else {
				this.GetComponent<Animator> ().speed = 1f;
				this.GetComponentInChildren<ParticleSystem> ().enableEmission = false;
			}

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                this.GetComponent<Transform>().position += Vector3.left * this.speed * (this.isBoosting ? this.boostMultiplier : 1) * Time.deltaTime;
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                this.GetComponent<Transform>().position += Vector3.right * this.speed * (this.isBoosting ? this.boostMultiplier : 1) * Time.deltaTime;
            }

            if (Input.GetKey(KeyCode.UpArrow))
            {
                this.GetComponent<Transform>().position += Vector3.up * this.speed * (this.isBoosting ? this.boostMultiplier : 1) * Time.deltaTime;
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                this.GetComponent<Transform>().position += Vector3.down * this.speed * (this.isBoosting ? this.boostMultiplier : 1) * Time.deltaTime;
            }

            if (Input.GetKeyDown(KeyCode.Space) && this.boostLeft >= 10f)
            {
                this.isBoosting = true;

				float randomFart = Random.Range (0f, 1.1f);

				if (randomFart > 0.5f) {
					this.fartSound1.Play ();
				} else {
					this.fartSound2.Play ();
				}

				this.blobSound.loop = true;
				this.blobSound.Play();

            }
            else if (Input.GetKey(KeyCode.Space) && this.boostLeft < 10f)
            {
				this.isBoosting = false;

				if(this.blobSound.isPlaying){
					this.blobSound.Stop ();
				}
            }

            if (Input.GetKeyUp(KeyCode.Space))
            {
				this.isBoosting = false;

				if(this.blobSound.isPlaying){
					this.blobSound.Stop ();
				}
            }

        }else {
            this.GetComponent<Transform>().Rotate(0f, 0f, this.speed * 3f);
        }
	
	}

	void OnCollisionEnter2D(Collision2D coll){

		string collName = coll.gameObject.name;

		switch (collName){

		case "hauki(Clone)":
			coll.gameObject.GetComponent<Hauki>().setSpeed(0f);
			this.GetComponent<Animator>().enabled = false;
			this.isGameOver = true;
			break;

		case "hukkunut1(Clone)":
			coll.gameObject.GetComponent<Hukkunut1>().setSpeed(0f);
			this.GetComponent<Animator>().enabled = false;
			this.isGameOver = true;
			break;

		case "muovipussi(Clone)":
			coll.gameObject.GetComponent<Muovipussi>().setSpeed(0f);
			this.GetComponent<Animator>().enabled = false;
			this.isGameOver = true;
			break;

		case "koukku":
			coll.gameObject.GetComponent<Rigidbody2D> ().isKinematic = true;
			this.GetComponent<Animator>().enabled = false;
			this.isGameOver = true;
			break;

		case "verkko(Clone)":
			if(!this.isBoosting){
				this.GetComponent<Animator>().enabled = false;
				this.isGameOver = true;
			}
			break;

		case "munkki(Clone)":
			Destroy (coll.gameObject);
			this.score += 10;
			this.lblScore.GetComponent<TextMesh> ().text = this.score.ToString ();
			this.GetComponent<Animator> ().SetBool ("isEating", true);
			this.eatingSound.Play ();
			break;

		case "sima(Clone)":
			Destroy (coll.gameObject);
			this.boostLeft += this.boostRegainOnDrink;
			this.score += 35;
			this.lblScore.GetComponent<TextMesh> ().text = this.score.ToString ();
			this.GetComponent<Animator> ().SetBool ("isEating", true);

			float randomBurb = Random.Range (0f, 1.1f);

			if (randomBurb > 0.5f) {
				this.burbSound1.Play ();
			} else {
				this.burbSound2.Play ();
			}
			break;

		}
	}

	void OnTriggerEnter2D(){
        this.isHiding = true;
	}

	void OnTriggerExit2D(){
        this.isHiding = false;
	}
}
