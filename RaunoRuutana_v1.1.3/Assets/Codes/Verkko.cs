﻿using UnityEngine;
using System.Collections;

public class Verkko : MonoBehaviour
{

    public float maxScale = 1f;
    public float minScale = 1f;
    public float maxPositionY = -1.12f;
    public float minPositionY = -1.12f;

    public float minRotation = 10f;

    public float maxRotation = 300f;

    public int spawnFrames = 5000;

    private GameObject kamera = null;

    // Use this for initialization
    void Start()
    {

        kamera = GameObject.Find("kamera");

        float scale = Random.Range(this.minScale, this.maxScale);
        this.GetComponent<Transform>().localScale = new Vector2(scale, scale);

        this.GetComponent<Transform>().position = new Vector2(Random.Range(this.kamera.GetComponent<Kamera>().objMaxSpawn, this.kamera.GetComponent<Kamera>().objSpawnLimit),
            Random.Range(this.minPositionY, this.maxPositionY));

        this.GetComponent<Transform>().Rotate(new Vector3(0f, 0f, Random.Range(this.minRotation, this.maxRotation)));


        float randomFlip = Random.Range(0f, 1.1f);

        if (randomFlip > 0.5f)
        {
            this.GetComponent<Transform>().Rotate(0f, 180f, 0f);
        }

    }

    // Update is called once per frame
    void Update()
    {

        this.GetComponent<Transform>().position += Vector3.left * (kamera.GetComponent<Kamera>().backgroundSpeed * kamera.GetComponent<Kamera>().backgroundSpeedFix) * Time.deltaTime;

        if (this.GetComponent<Transform>().position.x < kamera.GetComponent<Kamera>().objDestroyLimit)
        {
            Destroy(this.gameObject);
        }

    }

    public void setMaxScale(float maxScale)
    {
        this.maxScale = maxScale;
    }

    public float getMaxScale()
    {
        return this.maxScale;
    }

    public void setMinScale(float minScale)
    {
        this.minScale = minScale;
    }

    public float getMinScale()
    {
        return this.minScale;
    }

    public void setMinRotation(float minRotation)
    {
        this.minRotation = minRotation;
    }

    public float getMinRotation()
    {
        return this.minRotation;
    }

    public void setMaxRotation(float maxRotation)
    {
        this.maxRotation = maxRotation;
    }

    public float getMaxRotation()
    {
        return this.maxRotation;
    }

    public void setMaxPositionY(float maxPositionY)
    {
        this.maxPositionY = maxPositionY;
    }

    public float getMaxPositionY()
    {
        return this.maxPositionY;
    }

    public void setMinPositionY(float minPositionY)
    {
        this.minPositionY = minPositionY;
    }

    public float getMinPositionY()
    {
        return this.minPositionY;
    }

    public void setSpawnFrames(int spawnFrames)
    {
        this.spawnFrames = spawnFrames;
    }

    public int getSpawnFrames()
    {
        return this.spawnFrames;
    }
}
