﻿using UnityEngine;
using System.Collections;

public class Hukkunut1 : MonoBehaviour{

	public float maxScale = 0.5f;
	public float minScale = 0.4f;
	public float maxPositionY = 0.68f;
	public float minPositionY = -3.7f;
	public float rotateSpeed = 1f;
	public float speed = 1f;

    private int spawnFrames = 1000;

    private GameObject kamera = null;

    // Use this for initialization
    void Start()
    {

        kamera = GameObject.Find("kamera");

        float scale = Random.Range(this.minScale, this.maxScale);
        this.GetComponent<Transform>().localScale = new Vector2(scale, scale);

        this.GetComponent<Transform>().position = new Vector2(Random.Range(this.kamera.GetComponent<Kamera>().objMaxSpawn, this.kamera.GetComponent<Kamera>().objSpawnLimit),
            Random.Range(this.minPositionY, this.maxPositionY));

        float randomFlip = Random.Range(0f, 1.1f);

		if(randomFlip > 0.5f){
			this.GetComponent<Transform> ().Rotate(0f, 180f, 0f);
			this.rotateSpeed = -this.rotateSpeed;
		}

    }

    // Update is called once per frame
    void Update()
    {

        this.GetComponent<Transform>().position += Vector3.left * this.speed * Time.deltaTime;
        this.GetComponent<Transform>().Rotate(0f, 0f, this.rotateSpeed);

        if (this.GetComponent<Transform>().position.x < kamera.GetComponent<Kamera>().objDestroyLimit)
        {
            Destroy(this.gameObject);
        }

    }

    public void setMaxScale(float maxScale)
    {
        this.maxScale = maxScale;
    }

    public float getMaxScale()
    {
        return this.maxScale;
    }

    public void setMinScale(float minScale)
    {
        this.minScale = minScale;
    }

    public float getMinScale()
    {
        return this.minScale;
    }

    public void setMaxPositionY(float maxPositionY)
    {
        this.maxPositionY = maxPositionY;
    }

    public float getMaxPositionY()
    {
        return this.maxPositionY;
    }

    public void setMinPositionY(float minPositionY)
    {
        this.minPositionY = minPositionY;
    }

    public float getMinPositionY()
    {
        return this.minPositionY;
    }

    public void setSpawnFrames(int spawnFrames)
    {
        this.spawnFrames = spawnFrames;
    }

    public int getSpawnFrames()
    {
        return this.spawnFrames;
    }

    public void setRotateSpeed(float rotateSpeed) {
        this.rotateSpeed = rotateSpeed;
    }

    public float getRotateSpeed() {
        return this.rotateSpeed;
    }

    public void setSpeed(float speed)
    {
        this.speed = speed;
    }

    public float getSpeed()
    {
        return this.speed;
    }
}
