﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Pisteet : MonoBehaviour {

    private bool initSceneLoad = false;

    void OnMouseDown()
    {
        this.initSceneLoad = true;
    }

    void Start()
    {

    }


    void Update()
    {

        if (this.initSceneLoad)
        {
            if (!this.GetComponent<MenuSounds>().sound1.isPlaying && !this.GetComponent<MenuSounds>().sound2.isPlaying)
            {
                SceneManager.LoadScene("pisteskene");
            }
        }

    }
    void OnMouseOver()
    {
        this.GetComponent<SpriteRenderer>().color = new Color(255f, 172f, 0f, 13f);
    }
    void OnMouseExit()
    {
        this.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 255f);
    }
}
