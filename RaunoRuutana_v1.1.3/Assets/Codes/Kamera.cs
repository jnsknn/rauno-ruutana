﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Kamera : MonoBehaviour{

    public float objMaxSpawn = 12f;
    public float objMinSpawn = -12f;
    public float objSpawnLimit = 20f;
    public float objDestroyLimit = -20f;
    public float backgroundSpeed = 0.03f;
    public float backgroundSpeedFix = 50f;

    public AudioClip gameOverSound;
    private GameObject ruutana = null;

    private bool initGameOverSound = true;

	// Use this for initialization
	void Start () {
        this.ruutana = GameObject.Find("ruutana");
	}
	
	// Update is called once per frame
	void Update () {

        if (this.ruutana.GetComponent<Ruutana>().isGameOver && this.initGameOverSound) {
            this.backgroundSpeed = 0f;
            playGameOverSound();
        }

        if (!this.GetComponent<AudioSource>().isPlaying && !this.initGameOverSound){
            SceneManager.LoadScene("menuskene");
        }
	
	}

    private void playGameOverSound() {
        this.GetComponent<AudioSource>().clip = this.gameOverSound;
        this.GetComponent<AudioSource>().loop = false;
        this.GetComponent<AudioSource>().Play();
        this.initGameOverSound = false;
    }
}
