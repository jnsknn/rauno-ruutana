# Rauno Ruutana #

Rauno Ruutana is 2D game made with unity. Feel free to download source or ready built game from [Downloads](https://bitbucket.org/jnsknn/rauno-ruutana/downloads)!

This repository is only to share this project, it's source and end product. If you want to contribute contact me via email for more information.

The end product of this project was a winner of MAMK GAME KEVÄT 2016 game competition where all teams had 4 days to make a game from scratch with unity. There were a total of 7 teams.

The winning team consisted of three members: [Jonne Niskanen](https://bitbucket.org/jnsknn/), [Markus Tuominen](https://bitbucket.org/MarkusTuominen/) and [Roosa Penttinen](https://bitbucket.org/Roosa123/).

Published under Mozilla Public License 2.0.